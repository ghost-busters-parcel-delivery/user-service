package com.duelivotni.userservice.user.services;

import com.duelivotni.userservice.user.models.entities.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserService {
    User createUser(User user);

    Optional<User> getUserById(UUID userId);

    List<User> getAllUsers();

    User updateUser(User user);

    void deleteUserById(UUID userId);
}
