package com.duelivotni.userservice.user.services.impl;

import com.duelivotni.userservice.user.models.entities.User;
import com.duelivotni.userservice.user.repositories.UserRepository;
import com.duelivotni.userservice.user.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public User createUser(User user) {
        log.info("Creating new user: {}", user);
        return userRepository.save(user);
    }

    @Override
    public Optional<User> getUserById(UUID userId) {
        log.info("Getting user by id: {}", userId);
        return userRepository.findById(userId);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User updateUser(User user) {
        log.info("Updating user: {}", user);
        return userRepository.save(user);
    }

    @Override
    public void deleteUserById(UUID userId) {
        log.info("Deleting user with id: {}", userId);
        userRepository.deleteById(userId);
    }
}

