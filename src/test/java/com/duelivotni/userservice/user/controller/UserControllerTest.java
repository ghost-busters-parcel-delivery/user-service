package com.duelivotni.userservice.user.controller;

import com.duelivotni.userservice.user.models.entities.User;
import com.duelivotni.userservice.user.repositories.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@Testcontainers
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    @Container
    private static final PostgreSQLContainer<?> POSTGRESQL_CONTAINER = new PostgreSQLContainer<>("postgres:13")
            .withDatabaseName("user_service_db")
            .withUsername("user_service_user")
            .withPassword("user_service_password");

    @BeforeEach
    void setUp() {
        System.setProperty("spring.datasource.url", POSTGRESQL_CONTAINER.getJdbcUrl());
        System.setProperty("spring.datasource.username", POSTGRESQL_CONTAINER.getUsername());
        System.setProperty("spring.datasource.password", POSTGRESQL_CONTAINER.getPassword());
    }

    @AfterEach
    void tearDown() {
        userRepository.deleteAll();
        System.clearProperty("spring.datasource.url");
        System.clearProperty("spring.datasource.username");
        System.clearProperty("spring.datasource.password");
    }

    @Test
    void shouldCreateUser() throws Exception {
        User user = User.builder()
                .userId(UUID.randomUUID())
                .firstName("John")
                .lastName("Doe")
                .email("johndoe@example.com")
                .build();

        String userJson = objectMapper.writeValueAsString(user);

        mockMvc.perform(post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.userId").exists())
                .andExpect(jsonPath("$.firstName").value("John"))
                .andExpect(jsonPath("$.lastName").value("Doe"))
                .andExpect(jsonPath("$.email").value("johndoe@example.com"));
    }

    @Test
    void shouldGetUserById() throws Exception {
        User user = User.builder()
                .userId(UUID.randomUUID())
                .firstName("Bob")
                .lastName("Niger")
                .email("nigeria@gmail.com")
                .build();

        User userSaved = userRepository.save(user);

        mockMvc.perform(get("/api/users/{userId}", userSaved.getUserId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.userId").exists())
                .andExpect(jsonPath("$.firstName").value("Bob"))
                .andExpect(jsonPath("$.lastName").value("Niger"))
                .andExpect(jsonPath("$.email").value("nigeria@gmail.com"));
    }

    @Test
    void shouldGetAllUsers() throws Exception {
        User user1 = User.builder()
                .userId(UUID.randomUUID())
                .firstName("Lu")
                .lastName("Kahn")
                .email("johndoe@example.com")
                .build();

        User user2 = User.builder()
                .userId(UUID.randomUUID())
                .firstName("Kung")
                .lastName("Lao")
                .email("janedoe@example.com")
                .build();

        userRepository.saveAll(List.of(user1, user2));

        mockMvc.perform(get("/api/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].userId").exists())
                .andExpect(jsonPath("$[0].firstName").value("Lu"))
                .andExpect(jsonPath("$[0].lastName").value("Kahn"))
                .andExpect(jsonPath("$[0].email").value("johndoe@example.com"))
                .andExpect(jsonPath("$[1].userId").exists())
                .andExpect(jsonPath("$[1].firstName").value("Kung"))
                .andExpect(jsonPath("$[1].lastName").value("Lao"))
                .andExpect(jsonPath("$[1].email").value("janedoe@example.com"));
    }

    @Test
    void shouldUpdateUser_changedEmail() throws Exception {
        User user = User.builder()
                .userId(UUID.randomUUID())
                .firstName("Rebeca")
                .lastName("Jill")
                .email("jilly@example.com")
                .build();

        User userSaved = userRepository.save(user);

        userSaved.setFirstName("Rebeca");
        userSaved.setLastName("Jill");
        userSaved.setEmail("rebeca@example.com");

        String userJson = objectMapper.writeValueAsString(userSaved);

        mockMvc.perform(put("/api/users/{userId}", userSaved.getUserId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.userId").exists())
                .andExpect(jsonPath("$.firstName").value("Rebeca"))
                .andExpect(jsonPath("$.lastName").value("Jill"))
                .andExpect(jsonPath("$.email").value("rebeca@example.com"));
    }

    @Test
    void shouldDeleteUser() throws Exception {
        User user = User.builder()
                .userId(UUID.randomUUID())
                .firstName("John")
                .lastName("Doe")
                .email("johndoe@example.com")
                .build();

        User userSaved = userRepository.save(user);

        mockMvc.perform(delete("/api/users/{userId}", userSaved.getUserId()))
                .andExpect(status().isOk());

        assertFalse(userRepository.existsById(userSaved.getUserId()));
    }

    @Test
    void shouldReturnNotFoundWhenUserNotFound() throws Exception {
        UUID userId = UUID.randomUUID();
        mockMvc.perform(get("/api/users/{userId}", userId))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof RuntimeException))
                .andExpect(result -> assertTrue(Objects.requireNonNull(result.getResolvedException()).getMessage().contains("No user found with id: " + userId)));
    }
}



