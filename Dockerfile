FROM adoptopenjdk/openjdk11:alpine as build
WORKDIR /app
COPY . .
RUN ./gradlew clean build

FROM adoptopenjdk/openjdk11:alpine-jre
EXPOSE 8080
WORKDIR /app
COPY --from=build /app/build/libs/*.jar app.jar
CMD ["java", "-jar", "app.jar"]